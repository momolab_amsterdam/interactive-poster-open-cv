﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
public class KeyInputController : MonoBehaviour
{
    public PlayableAsset timelineSpotOut;
    public PlayableAsset timelineSpotIn;
    public PlayableAsset timelineDolly;
    public PlayableDirector director; 
    private bool daytime = false;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1) && director.state != PlayState.Playing) {
            if (daytime == true) {
                director.playableAsset = timelineSpotOut;
                director.Play();
                daytime = false;

            } else {
                director.playableAsset = timelineSpotIn;
                director.Play();
                daytime = true;
            }

        }
        if (Input.GetKeyDown(KeyCode.Alpha2) && director.state != PlayState.Playing) {
            director.playableAsset = timelineDolly;
            director.Play();
        }
    }
}
