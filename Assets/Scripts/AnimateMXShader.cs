﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
public class AnimateMXShader : MonoBehaviour
    
{

    public List<Texture2D> albedo;
    public List<Texture2D> heightMap;
    public List<Texture2D> emission;
    public Material material;

    private float timer = 0;
    private float cycleTime = 1.0f;
    private float gameTime = 1.0f;
    private int frameMultiplyer;

    private Material image;
    private List<Texture2D> sequence;
    private List<Texture2D> sequence_EM;
    private List<Texture2D> sequence_HM;
    private bool loop;
    private int index = 0;
    private bool isPlaying = false;

    private void Start() {
        Play(material, albedo, emission, heightMap, 30, true);
    }

    private void Update() {
        timer += Time.deltaTime;

        if (timer >= cycleTime) {
            timer -= cycleTime;

            //Do whatever you want to happen every cycle
            CycleEvent();
        }
    }
    public void Play(Material image, List<Texture2D> sequence, List<Texture2D> sequence_EM, List<Texture2D> sequence_HM,  float rate, bool loop, UnityEvent endOfAnimation = null) {
        cycleTime = 1.0f / rate;
        this.image = image;
        this.sequence = sequence;
        this.sequence_EM = sequence_EM;
        this.sequence_HM = sequence_HM;
        this.loop = loop;
        isPlaying = true;
        //StartCoroutine(Sequence(image, sequence, rate, loop, endOfAnimation));
    }
    public void CycleEvent() {
        gameTime = Time.deltaTime / 1.0f;
        frameMultiplyer = (int)(gameTime / cycleTime);
        //Debug.Log(frameMultiplyer);
        if (index >= sequence.Count) {
            index = 0;
        }
        if (albedo.Count > 0) {
            image.SetTexture("_MainTex", albedo[index]);
        }
        if (emission.Count > 0) {
            image.SetTexture("_Emission_Tex", emission[index]);
        }
        if (heightMap.Count > 0) {
            image.SetTexture("_BW_Normal_Tex", heightMap[index]);
            image.SetTexture("HeightMap_Tex", heightMap[index]);

        }
        
        
        
        if (frameMultiplyer > 1) {
            index = index + frameMultiplyer;
        } else {
            index++;
        }

    }

    IEnumerator Sequence(Material image, List<Texture2D> sequence, List<Texture2D> sequence_EM, List<Texture2D> sequence_HM, float rate, bool loop, UnityEvent e = null) {
        //sequence = sequence.OrderBy(w => w.id).ToList();
        for (int i = 0; i < sequence.Count; i++) {
            image.SetTexture("_MainTex", albedo[i]);
            yield return new WaitForSeconds((1f / rate));
        }
        if (e != null) {
            e.Invoke();
        }
        //yield return new WaitForEndOfFrame();

        if (loop) {
            StartCoroutine(Sequence(image, sequence, sequence_EM, sequence_HM, rate, loop));
        }

    }
}
