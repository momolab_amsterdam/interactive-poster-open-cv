﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour {
    public KinectManager manager;
    public Vector3 headPos = new Vector3(0, 0, 0);
    public GameObject cam;
    public float multiply;
    public GameObject target;
    public Vector3 sensorOffset;
    private Vector3 offset;
    private Vector3 headStartPos;
    public GameObject dad;

    // Start is called before the first frame update
    void Awake() {
        offset = cam.transform.position;
        headStartPos = manager.avatarControllers[0].GetJointWorldPos(KinectInterop.JointType.Head);

    }

    // Update is called once per frame
    void Update() {
        if (Input.GetKey(KeyCode.Z))
        {
            sensorOffset += new Vector3(0, 0.1f, 0);
        }
        if (Input.GetKey(KeyCode.X))
        {
            sensorOffset -= new Vector3(0, 0.1f, 0);
        }
        //Debug.Log(manager.avatarControllers.Count);
        //Debug.Log(manager.IsUserTracked(1));
        Debug.Log(manager.IsUserDetected());
        
        
        if (manager.avatarControllers.Count > 0 && manager.GetBodyCount() > 0) {
            
            if (manager.IsUserDetected() && manager.IsJointTracked(manager.GetPrimaryUserID(), manager.avatarControllers[0].GetBoneIndexByJoint(KinectInterop.JointType.Head, false)))
            {
                headPos = headStartPos - manager.avatarControllers[0].GetJointWorldPos(KinectInterop.JointType.Head);
                headPos = new Vector3(headPos.x * multiply, -headPos.y * multiply, headPos.z);
                Vector3 pos = offset + headPos;
                //Debug.Log(headPos);
                cam.transform.position = offset + sensorOffset + new Vector3(headPos.x, headPos.y, 0) + new Vector3(0, 0, dad.transform.position.z);
                //cam.transform.position = new Vector3(pos.x, pos.y, pos.z);
                //cam.transform.LookAt(target.transform.position);
            } else
            {
                Debug.Log("no controllor");
               // 

            }
            if (!manager.IsUserDetected())
            {
                //cam.transform.position = offset + sensorOffset + new Vector3(0, 0, dad.transform.position.z);
                cam.transform.position =  new Vector3(0, 0, dad.transform.position.z);
            }

        } else
        {
           


        }


    }
}
