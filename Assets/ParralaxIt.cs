﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using OpenCvSharp.Demo;

public class ParralaxIt : MonoBehaviour
{
    public FaceDetectorScene detector;
    private float x = 1;
    private float y = 1;
    public float multiply;
    public GameObject target;
    public float dampen;
    private float xVel;
    private float yVel;

    private Vector3 startPos;
    // Start is called before the first frame update
    void Start()
    {
        startPos = gameObject.transform.position;
    }


    private void Update() {
        //Debug.Log(detector.x);

        
        if ((float)detector.x != 0) {
            x = ((((float)detector.x / 600) - 0.5f) * multiply) + +startPos.x;
        }
        if ((float)detector.y != 0) {
            y = (((((float)detector.y / 550) - 0.5f) * -1) * multiply) + startPos.y;
        }


        
        float newX = Mathf.SmoothDamp(gameObject.transform.position.x, x, ref xVel, dampen);
        float newY = Mathf.SmoothDamp(gameObject.transform.position.y, y, ref yVel, dampen);
        //Debug.Log(newY);
        //Debug.Log(x);

        gameObject.transform.LookAt(target.transform);
      
            gameObject.transform.position = new Vector3(newX,  newY, gameObject.transform.position.z);
       
        
    }
}
